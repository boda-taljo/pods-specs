Pod::Spec.new do |s|

s.name         = 'BTGram'
s.version      = '0.1'
s.license      = { :type => 'agpl-3.0' }
s.homepage     = 'https://github.com/boda-taljo/BTGram'
s.authors      = { 'Boda Taljo' => 'me@aubada.com' }
s.summary      = 'A wrapper for Instagram APIs to provide better and easier to use interfaces for people building apps on Instagram'
s.source       = { :git => 'https://github.com/boda-taljo/BTGram.git', :tag => s.version }
s.source_files = 'BTGram/Source/**/*.{h,m}'
s.resources    = 'BTGram/Assets/**/*.{xib,png}'
s.framework = 'Foundation', 'UIKit'

s.platform     = :ios, '10.0'

end

